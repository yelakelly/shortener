# URL Shortener

Shorten, customize your links.
Demo - [80.211.65.150](http://80.211.65.150:8080/)

# Installation
Install dependencies
```
yarn (or npm install)
```

Build frontend
```
yarn build
```

Set environment variable with **your database connection link**

```
Example:
mysql://user:password@localhost:3306/database
```

Run:
```
yarn start
```

# Development

Run:
```
# Start development server
yarn dev (or npm run dev)
```


# Tested

```
Chrome 67.0.3396.99
Firefox 61.0.1
Edge 42.17134.1.0
Iphone 8 (Safari 11)
Android (Chrome)
```