exports.generateRandomString = function(stringLength = 8){
    const chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    let randomString = '';
    
	for (let i = 0; i < stringLength; i++) {
		let rNum = Math.floor(Math.random() * chars.length);
		randomString += chars.substring(rNum, rNum + 1);
    }
    
    return randomString;
}

exports.slugify = function slugify(text){
    return text.toLowerCase().replace(/ /g,'-').replace(/[-]+/g, '-').replace(/[^\w-]+/g,'');
}