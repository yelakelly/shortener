const express = require('express');
const bodyParser = require('body-parser');
const os = require('os');
const config = require('./config');

const app = express();
const port = process.env.PORT || config.port;

app.use(bodyParser.json());

app.use(express.static('dist'));
require("./app")(app);

require('./schedule.js');
app.listen(port, () => console.log('Listening on port 8080!'));
