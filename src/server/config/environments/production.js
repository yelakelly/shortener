module.exports = {
    host: "127.0.0.1",
    port: 8080,
    mysqlUrl: process.env.CONNECTION_STRING,
    logLevel: process.env.LOG_LEVEL,
};