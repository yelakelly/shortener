const request = require('request');
const utilities = require("../../utilities");
const {Link} = require('./models'); 

async function generateUntil(){
    let slug = utilities.generateRandomString();
    
    const link = await Link.findOne({
        where:{
            short_url: slug
        }
    });

    if(link){
        return generateUntil();
    } else {
        return slug;
    }
}

exports.create = async(req, res) => {
    try {
        let { url, short_url } = req.body;
        let generatedUrl;
        short_url = utilities.slugify(short_url.substring(0, 12));

        if(!short_url){
            generatedUrl = await generateUntil();
        }

        request.get(url, {timeout: 2000}, async (err, result, body) => {
            if (err) { 
                let errorMessage = err.code === 'ETIMEDOUT' ? 'Server Not Found' : 'Please enter a valid URL';

                res.send( {
                    'error': err,
                    'message': errorMessage
                });
                
                return;
            }

            if(result && result.statusCode == 200){
                let fullLink, checkExistLink;

                if(!generatedUrl){
                    checkExistLink = await Link.findOne({
                        where:{
                            short_url: short_url
                        }
                    });
                }

                if(checkExistLink){
                    res.send( {
                        'error': 'URL already exists',
                        'message': `Please choose other short link. This short link '${short_url}' already exists.`
                    });
                } else {
                    Link.create({
                        'url': url,
                        'short_url': generatedUrl ? generatedUrl : short_url,
                    }).then((link) => {
                        res.json(link)
                    });
                }
            }
       });
    } catch ( err ) {
        res.send( {
            'error': err.message
        } );
    }
};

exports.detail = async(req, res) => {
    try{
        const slug = req.params.slug;
        const link = await Link.findOne({
            where:{
                short_url: slug
            }
        });

        if(link){
            res.json(link.dataValues);
        } else{
            res.status(404).send('404: Not found'); 
        }
    } catch ( err ) {
        res.send( "Error: " + err.message);
    }
};

exports.delete = (req, res) => {
    try{
        const slug = req.params.slug;
        Link.destroy({
            where:{
                short_url: slug
            } 
        }).then((id) => {
            if(id){
                res.status(202).json({
                    message: "Success",
                    status: 204,
                });;
            } else {
                res.status(204).json({
                    message: "Instance with such ID doesn't exist",
                    status: 204,
                });
            }
        })
    } catch ( err ) {
        res.send( "Error: " + err.message);
    }
};

exports.redirect = async(req, res) => {
    try {
       const slug = req.params.slug;
       const link = await Link.findOne({
           where:{
               short_url: slug
           }
       });

       if(link){
            res.status(302).redirect(link.url);
            link.count = link.count += 1;
            link.save();
       } else{
            res.status(404).send('404: Not found'); 
       }
       
    } catch ( err ) {
        res.send( "Error: " + err.message );
    }
};