const controller = require("./controller");

const express = require("express");
const router = express.Router();

router.post("/api/", controller.create);
router.get("/api/:slug", controller.detail);
router.delete("/api/:slug", controller.delete);

router.get( "/:slug", controller.redirect);

module.exports = router;