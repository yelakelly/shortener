module.exports = (sequelize, type) => {
  return sequelize.define('link', {
      id: {
        type: type.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      url: {
        type: type.TEXT,
        allowNull: false,
      },
      short_url: {
        type: type.STRING(50),
        allowNull: false,
      },
      count: {
        type: type.INTEGER,

        defaultValue: 0
      }
  })
}