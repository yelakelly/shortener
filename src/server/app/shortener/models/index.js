const Sequelize = require('sequelize');
const LinkModel = require('./link')
const config = require('../../../config');

const sequelize = new Sequelize(config.mysqlUrl, {
    host: 'localhost',
  
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
    operatorsAliases: false
});

const Link = LinkModel(sequelize, Sequelize);

sequelize.sync()

module.exports = {
    Link
}