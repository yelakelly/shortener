const shortenerRouter = require("./shortener/router");

module.exports = (app) => {
    app.use("/", shortenerRouter);
};