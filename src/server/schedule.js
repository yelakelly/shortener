const Sequelize = require('sequelize');
const schedule = require('node-schedule');
const {Link} = require('./app/shortener/models'); 

const Op = Sequelize.Op;

const LinkCleanTime = ((h, m, s) => {
    let rule = new schedule.RecurrenceRule();
    rule.hour = h;
    rule.minute = m;
    rule.second = s;
    return rule;
})(24, 0, 0);

const LinkCleanDayInterval = 5;

module.exports = function(){
    let LinkCleaner = schedule.scheduleJob(LinkCleanTime, function(fireDate){
        Link.destroy({
            where:{
                createdAt: {
                    [Op.lt]: new Date() - ((24 * 60 * 60 * 1000) * LinkCleanDayInterval)
                }
            } 
        })
        console.log('Running clean up links ' + fireDate);
    });
}