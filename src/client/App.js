import React, { Component } from "react";

import "bootstrap/dist/css/bootstrap.min.css";
import "./app.css";

class Form extends Component{
  render(){
    return (
      <form className="app-main-form" onSubmit={(e) => this.props.onSubmit(e)}>
        <div className="input-group">
          <input type="text" className="form-control" value={this.props.url} onChange={(e) => this.props.onChangeLink(e)} placeholder="Write a link to shorten it"/>
          <div className="input-group-append">
            <button className="btn btn-secondary" type="submit">
              Shorten
            </button>
          </div>
        </div>
        <div className="app-main-form__slug-wrapper">
          <div className="app-main-form__slug">
            <p className="app-main-form__helper">
              Customize your link name (optional)
            </p>
            <div className="input-group">
              <input type="text" value={this.props.short_url} onChange={(e) => this.props.onChangeShortUrl(e)} className="form-control" placeholder="Your custom link name"/>
            </div>
            <span className="text-muted app-main-form__slug-warning">
              <sup>*</sup> Only Latin letters and numbers allowed
            </span>
          </div>
        </div>
      </form>
    )
  }
}

function LinkGroup(props){
    return (
      <div className="list-group">
        {
          props.links.map((link, index) => {
            return (
              <div className="list-group-item list-group-item-action flex-column align-items-start" key={index}>
                <a href={link.short_url} className="h5 mb-1 d-inline-block">{link.short_url}</a>
                <div>
                  <a href={link.link}><small className="text-muted d-inline-block app-link-full">{link.url}</small></a>
                </div>
              </div>
            )
          })
        }
      </div>
    )
}


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      url: "",
      short_url: "",
      links: [],
      errorMessage: ''
    };
  }

  handleSubmit(e){
    e.preventDefault();

    if(!this.state.url){
      this.setState({
        'errorMessage': 'URL is empty'
      })
      return;
    }

    fetch("/api/", {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify({
          'url': this.state.url,
          'short_url': this.state.short_url
        }),
    })
    .then(response => {
      return response.json()
    }).then(response => {
      let errorMessage = '';
      let links = this.state.links.slice();

      if(response.error){
        if(response.message === undefined){
          console.log(response.error);
          return;
        } else{
          errorMessage = response.message;
        }
      } else{
        links = links.concat([
          response  
        ]);
      }

      this.setState({
        url: "",
        short_url: "",
        links: links,
        errorMessage
      });
    });
  }

  onChangeLink(e){
    this.setState({
      url: e.target.value
    });
  }

  onChangeShortUrl(e){
    this.setState({
      short_url: slugify(e.target.value).substring(0, 12)
    });
  }

  render() {
    return (
      <div className="container">
        <div className="site-inner">
          <div className={this.state.errorMessage ? 'alert alert-danger' : ''} role="alert">
            {this.state.errorMessage}
          </div>
          <Form 
            onChangeLink={(i) => this.onChangeLink(i)} 
            onChangeShortUrl={(i) => this.onChangeShortUrl(i)} 
            onSubmit={(e) => {this.handleSubmit(e)}} 
            url={this.state.url}
            short_url={this.state.short_url}
          />
          <LinkGroup links={this.state.links}/>
        </div>
      </div>
    );
  }
}

function slugify(text){
  return text.toLowerCase().replace(/ /g,'-').replace(/[-]+/g, '-').replace(/[^\w-]+/g,'');
}